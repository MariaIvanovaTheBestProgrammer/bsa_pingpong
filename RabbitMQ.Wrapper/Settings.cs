﻿namespace RabbitMQ.Wrapper
{
    public class Settings
    {
        public Settings(string queueName, string exchangeName, string routingKey)
        {
            QueueName = queueName;
            ExchangeName = exchangeName;
            RoutingKey = routingKey;
        }
        public string QueueName { get; set; }
        public string ExchangeName { get; set; }
        public string RoutingKey { get; set; }
    }
}
