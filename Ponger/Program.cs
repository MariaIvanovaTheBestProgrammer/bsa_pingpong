﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace Ponger
{
    class Program
    {
        static Wrapper wrapper;
        private static void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"{message} {DateTime.Now}");
            Thread.Sleep(2500);
            wrapper.SendMessageToQueue("pong");
        }
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            var configuration = builder.Build();
            Settings settings = new Settings("ping_queue", "BetaExchange", "ping_queue");

            wrapper = new Wrapper(configuration, settings);
            wrapper.ListenQueue(MessageReceived);
            Console.ReadLine();
        }
    }
}
