﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQ.Wrapper
{
    public delegate void ListenerMessageHandler(object sender, BasicDeliverEventArgs args);
    public class Wrapper : IDisposable 
    {
        private IModel _channel;
        private IConnection _connection;

        public Wrapper(IConfiguration config, Settings settings)
        {
            var factory = new ConnectionFactory()
            {
                HostName = config.GetSection("RabbitHost").Value
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            Settings = settings;
        }
        public Settings Settings { get; set; }
        public void SendMessageToQueue(string message)
        {
            SetUpExchangeAndQueue();

            var body = Encoding.UTF8.GetBytes(message);
            _channel.BasicPublish(exchange: Settings.ExchangeName,
                                    routingKey: Settings.QueueName,
                                    basicProperties: null,
                                    body: body);
        }

        public void ListenQueue(ListenerMessageHandler message)
        {
            SetUpExchangeAndQueue();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (s, a) => message(s, a);
            consumer.Received += (s, a) => _channel.BasicAck(a.DeliveryTag, false);

            _channel.BasicConsume(queue: Settings.QueueName,
                                    autoAck: false,
                                    consumer: consumer);
        }

        private void SetUpExchangeAndQueue()
        {
            EnsureExchangeCreated();
            EnsureQueueCreated();
            _channel.QueueBind(Settings.QueueName, Settings.ExchangeName, Settings.QueueName);
        }

        private void EnsureQueueCreated()
        {
            _channel.QueueDeclare(queue: Settings.QueueName,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
        }

        private void EnsureExchangeCreated()
        {
            _channel.ExchangeDeclare(Settings.ExchangeName, ExchangeType.Direct);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}
