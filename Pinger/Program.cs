﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper;

namespace Pinger
{
    public class Program
    {
        static Wrapper wrapper;
        private static void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"{message} {DateTime.Now}");
            Thread.Sleep(2500);
            wrapper.SendMessageToQueue("ping");
        }

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            var configuration = builder.Build();
            Settings settings = new Settings("pong_queue", "BetaExchange", "pong_queue");

            wrapper = new Wrapper(configuration, settings);
            wrapper.SendMessageToQueue("ping");
            wrapper.ListenQueue(MessageReceived);
            Console.ReadLine();
        }
    }
}
